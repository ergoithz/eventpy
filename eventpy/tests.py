
import unittest

import eventpy


class CommonTargetMixin(object):
    def test_repr(self):
        prefix = '%s(' % self.target.__name__
        result = repr(self.target())
        self.assertEqual(result[:len(prefix)], prefix)


class EventTest(CommonTargetMixin, unittest.TestCase):
    module = eventpy
    target = eventpy.Event

    def setUp(self):
        self.event = self.target()

    def test_call(self):
        a = []
        self.event.append(a.append)
        self.event.append(lambda x: a.append(x + 1))
        self.event(1)
        self.assertListEqual(a, [1, 2])

    def test_once(self):
        a = []
        once = self.module.once
        self.event.append(once(a.append))
        self.event(1)
        self.event(2)
        self.event.append(once(a.append))
        self.event.insert(0, once(lambda x: a.append(repr(x))))
        self.event(1)
        self.event(2)
        self.assertListEqual(a, [1, '1', 1])

    def test_bound(self):
        a = []
        bound = self.module.bound
        self.event.append(bound(a.append))
        self.event()
        self.assertListEqual(a, [self.event])

    def test_once_bound(self):
        a = []
        once = self.module.once
        bound = self.module.bound
        self.event.append(once(bound(a.append)))
        self.event()
        self.event()
        self.assertListEqual(a, [self.event])

    def test_once_identity(self):
        once = self.module.once
        handler = self.test_once_identity
        ref = once(handler)
        self.event.append(ref)
        self.assertNotIn(handler, self.event)
        self.assertIn(ref, self.event)

    def test_once_remove(self):
        a = []

        def handler():
            self.event.remove(handler_once)
            a.append(1)

        handler_once = self.module.once(handler)

        self.event.append(handler)
        self.event.append(handler_once)

        self.event()
        self.assertListEqual(a, [1])

    def test_stop(self):
        a = []

        def handler(x):
            a.append(x + 1)
            raise self.module.StopEvent()

        self.event.append(handler)
        self.event.append(a.append)
        self.event(1)
        self.assertListEqual(a, [2])

    def test_thread_safety(self):
        a = []
        self.event.append(lambda x: self.event(x + 1) if x < 2 else None)
        self.event.append(a.append)
        self.event.append(a.append)
        self.event(1)
        self.assertListEqual(a, [1, 1, 2, 2])


class EventManagerTest(CommonTargetMixin, unittest.TestCase):
    module = eventpy
    target = eventpy.EventManager

    def setUp(self):
        self.events = self.target()

    def test_subscribe(self):
        a = []
        self.events['test'].append(a.append)
        self.events['test'].append(a.append)
        self.events['test'](1)
        self.events['test'](2)
        self.assertListEqual(a, [1, 1, 2, 2])

    def test_manager(self):
        self.assertIs(
            self.module.manager(),
            self.module.manager()
            )
        hashable = type('Type', (object,), {})
        self.assertIs(
            self.module.manager(hashable),
            self.module.manager(hashable)
            )

    def test_handler(self):
        a = []

        @self.events.handler('fnctest')
        @self.module.once
        def handler(c):
            a.append(c)

        self.events.emit('fnctest', 1)
        self.events.emit('fnctest', 2)

        self.assertListEqual(a, [1])

    def test_functions(self):
        a = []

        @self.module.handler('fnctest')
        @self.module.handler('ns.fnctest')
        def handler(c):
            a.append(c)

        self.module.emit('fnctest', 1)
        self.module.emit('ns.fnctest', 2)

        self.assertListEqual(a, [1, 2])
