'''
eventpy
-------

Thread-safe minimal event and event-manager implementation.

This is the main (and sole public) eventpy module.

This module exposes:

  * Callable list type :class:`Event`.
  * Default-dict type :class:`EventManager`
  * Event stopping exception :exc:`StopEvent`
  * Convenience function :func:`manager` to retrieve :class:`EventManager`
    instances by name.
  * Convenience function :func:`emit` to call :class:`Event` instances by name.
  * Function decorator :func:`handler` to append functions to :class:`Event`
    instances by name.
  * Function decorator :func:`bound` to tell :class:`Event` to pass its own
    reference as event handler first argument.
  * Function decorator :func:`once` to remove handler itself from caller
    :class:`Event` instance when executed.

Please refer to :func:`help` or project's README.rst and documentation for
further info.
'''
import threading
import collections
import functools


__all__ = (
    'Event', 'EventManager', 'manager', 'handler', 'emit',
    'once', 'bound', 'StopEvent'
    )


class Event(list):
    '''
    Event handler callable list.

    A event handler can be any callable object (like a function).

    Calling this (as :meth:`__call__` is defined) will execute event handlers
    in order, passing given arguments to all handlers.

    Usage:
    >>> def f(x):
    ...     print 'f(%s)' % x
    >>> def g(x):
    ...     print 'g(%s)' % x
    >>> e = Event()
    >>> e()
    >>> e.append(f)
    >>> e(123)
    f(123)
    >>> e.remove(f)
    >>> e()
    >>> e += (f, g)
    >>> e(10)
    f(10)
    g(10)
    >>> del e[0]
    >>> e(2)
    g(2)
    '''

    lock_class = threading.Lock
    queue_class = collections.deque
    bound_attribute = 'eventpy_bound'

    def __init__(self, iterable=()):
        '''
        :param iterable: initial value
        :type iterable: iterable
        '''
        self._lock = self.lock_class()
        self._queue = self.queue_class()
        super(Event, self).__init__(iterable)

    def __call__(self, *args, **kwargs):
        '''
        Execute all event handlers with given positional and keyword arguments.

        The handler execution will be queued, returning immediately,
        if this event is already being processed to avoid deadlocks. So, under
this case, the call is technically asynchronous.

        Queued executions are usually performed by the current blocking call,
        after its handlers, in the same thread, thus delaying its finalization.

        This design makes this library compatible with single-threaded
        environments like green-threaded ones.
        '''
        self._queue.append((args, (self,) + args, kwargs))
        while self._queue:
            if self._lock.acquire(False):
                try:
                    hattr = hasattr
                    battr = self.bound_attribute
                    uargs, bargs, kwargs = self._queue.popleft()
                    for f in self[:]:
                        args = bargs if hattr(f, battr) else uargs
                        f(*args, **kwargs)
                except StopEvent:
                    pass
                finally:
                    self._lock.release()
            else:
                break

    def __repr__(self):
        return "Event(%s)" % list.__repr__(self)


class EventManager(collections.defaultdict):
    '''
    Dict-like of :class:`Event` objects.

    Usage:
    >>> def f(x):
    ...     print 'f(%s)' % x
    >>> def g(x):
    ...     print 'g(%s)' % x
    >>> m = EventManager()
    >>> 'e' in m
    False
    >>> m.e.append(f)
    >>> 'e' in m
    True
    >>> m['e'](123)
    f(123)
    >>> m['e'].remove(f)
    >>> m['e']()
    >>> m['e'] += (f, g)
    >>> m['e'](10)
    f(10)
    g(10)
    >>> del m['e'][0]
    >>> m['e'](2)
    g(2)
    '''
    def __init__(self, factory=Event, *args, **kwargs):
        super(EventManager, self).__init__(factory, *args, **kwargs)

    def __repr__(self):
        return "EventManager(%s)" % dict.__repr__(self)

    def handler(self, event_name):
        '''
        Handler function decorator.

        Usage:

        >>> manager = EventManager()
        >>> @manager.handler('my_event_name')
        ... def my_event_handler_function(arg1, arg2):
        ...     print(arg1, arg2)

        :param event_name: event name
        :type event_name: str
        :returns: decorator function
        :rtype: function
        '''
        event = self[event_name]

        def decorator(fnc):
            '''
            Store given function as event handler.

            :param fnc: function
            :type fnc: function
            :returns: given function
            :rtype: function
            '''
            event.append(fnc)
            return fnc
        return decorator

    def emit(self, event_name, *args, **kwargs):
        '''
        Emit an event.

        All positional and keyword arguments passed after `event_name` are
        proxied to all event handlers.

        :param event_name: event name
        :type event_name: str
        '''
        self[event_name](*args, **kwargs)


class StopEvent(StopIteration):
    '''
    Exception wich will stop current event handler calls.
    '''
    pass


def manager(name=None, registry=collections.defaultdict(EventManager)):
    '''
    Get :class:`EventManager` instance based on given optional name.

    :param name: event manager name in the registry, defaults to None
    :type name: hashable
    :param registry: mapping, defaults to a global EventManager defaultdict
    :type registry: mapping
    :returns: EventManager instance
    :rtype: EventManager
    '''
    return registry[name]


def resolve_event_name(event_name):
    '''
    Global event-name resolver.

    :param event_name: event name
    :type event_name: str
    :returns: tuple with event manager and real event_name
    :rtype: tuple (EventManager, str)
    '''
    if '.' in event_name:
        manager_name, event_name = event_name.rsplit('.', 1)
        return manager(manager_name), event_name
    return manager(), event_name


def handler(event_name):
    '''
    Handler function decorator.

    If event_name contains a dot '.' character, everything before is assumed
    as a global event manager name.

    Usage:

    >>> @handler('my_event_name')
    ... def my_event_handler_function(arg1, arg2):
    ...     print(arg1, arg2)

    :param event_name: event name
    :type event_name: str
    :returns: decorator function
    :rtype: function
    '''
    manager, event_name = resolve_event_name(event_name)
    return manager.handler(event_name)


def emit(event_name, *args, **kwargs):
    '''
    Event emitter convenience function.

    If event_name contains a dot '.' character, everything before is assumed
    as a global event manager name.

    All positional and keyword arguments passed after `event_name` are
    proxied to all event handlers.

    :param event_name: event name
    :type event_name: str
    '''
    manager, event_name = resolve_event_name(event_name)
    manager.emit(event_name, *args, **kwargs)


def bound(fnc, event_class=Event):
    '''
    Event handler decorator telling events this handler must receive the event
    object itself as first argument.

    Attribute used to mark function as "bound" is taken from
    :cvar:`Event.bound_attribute` (or from the same attribute of class
    given as event_class argument).

    :param fnc: event handler
    :type fnc: callable
    :param event_class: optional event_class, defaults to eventpy.Event
    :type event_class: type
    :returns: decorated function
    :rtype: function
    '''
    @functools.wraps(fnc)
    def wrapper(event, *args, **kwargs):
        return fnc(event, *args, **kwargs)
    wrapper.wrapped = fnc
    setattr(wrapper, event_class.bound_attribute, None)
    return wrapper


def once(fnc, event_class=Event):
    '''
    Event handler decorator telling events this handler must be
    removed when executed.

    Attribute used to mark function as "bound" is taken from
    :cvar:`Event.bound_attribute` (or from the same attribute of class
    given as event_class argument).

    :param fnc: event handler
    :type fnc: callable
    :param event_class: optional event_class, defaults to eventpy.Event
    :type event_class: type
    :returns: decorated function
    :rtype: function
    '''
    @functools.wraps(fnc)
    def wrapper(event, *args, **kwargs):
        try:
            event.remove(wrapper)
        except ValueError:
            return  # assume already executed
        if hasattr(fnc, event.bound_attribute):
            return fnc(event, *args, **kwargs)
        return fnc(*args, **kwargs)
    wrapper.wrapped = fnc
    setattr(wrapper, event_class.bound_attribute, None)
    return wrapper
