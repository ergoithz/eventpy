eventpy
=======

.. image:: https://img.shields.io/travis/ergoithz/eventpy/master.svg
  :target: https://travis-ci.org/ergoithz/eventpy
  :alt: Travis-CI badge

.. image:: http://img.shields.io/coveralls/ergoithz/eventpy/master.svg
  :target: https://coveralls.io/r/ergoithz/eventpy
  :alt: Coveralls badge

.. image:: https://img.shields.io/codacy/grade/dd0981ddacf84e3ea18c9dbad375c011/master.svg
  :target: https://www.codacy.com/app/ergoithz/eventpy
  :alt: Codacy badge

Thread-safe minimal (~75 code lines) event and event-manager
implementation.

Motivation
----------

I needed a simple (and well tested) general purpose EventManager implementation
for a current project, and besides a lot of both pub-sub and very rigid
framework-specific libraries, there isn't any.

Documentation
-------------

Well, aside from this README, there isn't, but it will be available soon.

Examples
--------

Global default event-manager.

.. code-block:: python

    from eventpy import handler, emit

    @handler('myevent')
    def myhandler(parameter)
        print(parameter)

    emit('myevent', 'myparameter')
    # prints myparameter

Global named event manager.

.. code-block:: python

    from eventpy import handler, emit

    @handler('mymanager.myevent')
    def myhandler(parameter)
        print(parameter)

    emit('mymanager.myevent', 'myparameter')
    # prints myparameter

Or alternatively...

.. code-block:: python

    from eventpy import manager

    manager = manager('mymanager')

    @manager.handler('myevent')
    def myhandler(parameter)
        print(parameter)

    manager.emit('myevent', 'myparameter')
    # prints myparameter

Local (private) event manager.

.. code-block:: python

    from eventpy import EventManager

    manager = EventManager()

    @manager.handler('myevent')
    def myhandler(parameter)
        print(parameter)

    manager.emit('myevent', 'myparameter')
    # prints myparameter
