# -*- coding: utf-8 -*-

import os
import os.path
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

sys_path = sys.path[:]
sys.path[:] = (os.path.abspath('eventpy'),)
__import__('__meta__')
sys.path[:] = sys_path

meta = sys.modules['__meta__']
meta_app = meta.__app__
meta_version = meta.__version__
meta_license = meta.__license__
meta_description = meta.__description__

with open('README.rst') as f:
    meta_doc = f.read()

setup(
    name=meta_app,
    version=meta_version,
    description=meta_description,
    long_description=meta_doc,
    license=meta_license,
    author='Felipe A. Hernandez',
    author_email='ergoithz@gmail.com',
    url='https://github.com/ergoithz/eventpy',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    keywords=['event'],
    packages=['eventpy'],
    test_suite='eventpy.tests',
    platforms='any'
)
